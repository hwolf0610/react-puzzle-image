import logo from "./logo.svg";
import "./App.css";
import Puzzle from "react-image-puzzle-touch";
import PuzzleImage from "./3.png";

function App() {
  return (
    <div
      className="App"
      style={{
        position: "fixed",
        height: "100%",
        width: "100%",
        overflow: "none",
      }}
    >
      <Puzzle
        image={PuzzleImage}
        size={800}
        level={4}
        onDone={() => {
          alert("done");
        }}
      />
    </div>
  );
}

export default App;
